<?php
/**
 * @file
 * The hooks that are needed for integration with the views api are included
 * in this file.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Implementation of hook_views_data().
 *
 * @return array
 */
function watchdog_aggregator_views_data() {
  $data = array(
    'watchdog_aggregator_sites' => array(
      'table' => array(
        'group' => t('aggregated logs'),
      ),
      'sid' => array(
        'title' => t('sid'),
        'help' => t('The ID of a website.'),
      ),
      'url' => array(
        'title' => t('URL'),
        'help' => t('The url of the website.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'active' => array(
        'title' => t('Active'),
        'help' => t('Indicates if the log aggregation for this website is active.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
    ),
    'watchdog_aggregator_logs' => array(
      'table' => array(
        'group' => t('aggregated logs'),
        'base' => array(
          'field' => 'wid',
          'title' => t('Aggregated log entry'),
          'help' => t('Log entries from other websites that were collected by the watchdog aggregator module.'),
        ),
      ),
      'sid' => array(
        'title' => t('Website'),
        'help' => t('The website the log entry originates from.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'type' => array(
        'title' => t('Type'),
        'help' => t('Type of log message, for example "user" or "page not found."'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'message' => array(
        'title' => t('Message'),
        'help' => t('Text of log message to be passed into the t() function.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'variables' => array(
        'title' => t('Variables'),
        'help' => t('Serialized array of variables that match the message string and that is passed into the t() function.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'severity' => array(
        'title' => t('Severity'),
        'help' => t('The severity level of the event; ranges from 0 (Emergency) to 7 (Debug)'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'link' => array(
        'title' => t('Link'),
        'help' => t('Link to view the result of the event.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'location'  => array(
        'title' => t('Location'),
        'help' => t('URL of the origin of the event.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'referer' => array(
        'title' => t('Referer'),
        'help' => t('URL of referring page.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'hostname' => array(
        'title' => t('Hostname'),
        'help' => t('Hostname of the user who triggered the event.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
      ),
      'timestamp' => array(
        'title' => t('Timestamp'),
        'help' => t('Unix timestamp of when event occurred.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
    ),
  );

  return $data;
} // function watchdog_aggregator_views_data
