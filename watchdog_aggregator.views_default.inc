<?php
/**
 * @file
 * This file contains the default views for the watchdog aggregator module.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Implementation of hook_views_default_views().
 *
 * @return array
 */
function watchdog_aggregator_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'latest_aggregated_log_entries';
  $view->description = 'This view shows the latest aggregated log entries.';
  $view->tag = 'default';
  $view->view_php = '';
  $view->base_table = 'watchdog_aggregator_logs';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = TRUE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Standards', 'default');
  $handler->override_option('fields', array(
  'timestamp' => array(
    'label' => 'Timestamp',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'timestamp',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'timestamp',
    'relationship' => 'none',
  ),
  'hostname' => array(
    'label' => 'Hostname',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'hostname',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'hostname',
    'relationship' => 'none',
  ),
  'message' => array(
    'label' => 'Message',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'message',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'message',
    'relationship' => 'none',
  ),
  'location' => array(
    'label' => 'Location',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'location',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'location',
    'relationship' => 'none',
  ),
  'referer' => array(
    'label' => 'Referer',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'referer',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'referer',
    'relationship' => 'none',
  ),
  'severity' => array(
    'label' => 'Severity',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'severity',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'severity',
    'relationship' => 'none',
  ),
  'type' => array(
    'label' => 'Type',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'type',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'type',
    'relationship' => 'none',
  ),
  'sid' => array(
    'label' => 'Website',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'sid',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'sid',
    'relationship' => 'none',
  ),
  'variables' => array(
    'label' => 'Variables',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 1,
    'id' => 'variables',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'variables',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('sorts', array(
  'timestamp' => array(
    'order' => 'DESC',
    'id' => 'timestamp',
    'table' => 'watchdog_aggregator_logs',
    'field' => 'timestamp',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('access', array(
  'type' => 'none',
  ));
  $handler->override_option('cache', array(
  'type' => 'none',
  ));
  $handler->override_option('items_per_page', 25);

  $views[$view->name] = $view;

  return $views;
} // function watchdog_aggregator_views_default_views