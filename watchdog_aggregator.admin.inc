<?php
/**
 * @file
 * This file contains all code for forms and settings regarding the
 * watchdog aggregator module.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Generate the form for a site configuration.
 *
 * @param array $form_state
 * @param array $site
 * @return array
 */
function watchdog_aggregator_admin_form_add_site($form_state, $site = array()) {
  $form = array();

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => isset($site['sid']) ? $site['sid'] : 0,
  );
  $form['url'] = array(
    '#title' => t('URL'),
    '#description' => t('Please enter the url of your website here (http://example.com).'),
    '#type' => 'textfield',
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => isset($site['url']) ? $site['url'] : '',
  );
  $form['dbhost'] = array(
    '#title' => t('Database host'),
    '#description' => t('Enter the url of your database server here (db.example.com).'),
    '#type' => 'textfield',
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => isset($site['dbhost']) ? $site['dbhost'] : '',
  );
  $form['dbport'] = array(
    '#title' => t('Database port'),
    '#description' => t('Enter the port your database server is listening on. You may stick with "0" for the default port.'),
    '#type' => 'textfield',
    '#maxlength' => 5,
    '#required' => TRUE,
    '#default_value' => isset($site['dbport']) ? $site['dbport'] : 0,
  );
  $form['dbtype'] = array(
    '#title' => t('Database type'),
    '#description' => t('Select the type of your database here. Beware that mysqli is the recommended way to connect to MySQL.'),
    '#type' => 'select',
    '#options' => array(
      'mysqli' => t('MySQL via mysqli'),
      'mysql' => t('MySQL'),
      'pgsql' => t('PostgreSQL'),
    ),
    '#required' => TRUE,
    '#default_value' => isset($site['dbtype']) ? $site['dbtype'] : 'mysqli',
  );
  $form['dbname'] = array(
    '#title' => t('Database name'),
    '#description' => t('Enter the name of your drupal database here.'),
    '#type' => 'textfield',
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => isset($site['dbname']) ? $site['dbname'] : '',
  );
  $form['dbuser'] = array(
    '#title' => t('Database user'),
    '#description' => t('The username for database access.'),
    '#type' => 'textfield',
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => isset($site['dbuser']) ? $site['dbuser'] : '',
  );
  $form['dbpass'] = array(
    '#title' => t('Database password'),
    '#description' => t('The password for database access.'),
    '#type' => 'password',
    '#maxlength' => 254,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
} // function watchdog_aggregator_admin_form_add_site

/**
 * Submit function for watchdog_aggregator_admin_form_add_site.
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function watchdog_aggregator_admin_form_add_site_submit($form, &$form_state) {
  $url = (string)trim($form_state['values']['url']);
  $dbhost = (string)trim($form_state['values']['dbhost']);
  $dbport = (int)trim($form_state['values']['dbport']);
  $dbtype = (string)trim($form_state['values']['dbtype']);
  $dbname = (string)trim($form_state['values']['dbname']);
  $dbuser = (string)trim($form_state['values']['dbuser']);
  $dbpass = (string)trim($form_state['values']['dbpass']);
  $sid = (int)trim($form_state['values']['sid']);
  if ($sid === 0) {
    watchdog_aggregator_add_site($url, $dbhost, $dbport, $dbtype, $dbname, $dbuser, $dbpass);
  }
  else {
    watchdog_aggregator_update_site($sid, $url, $dbhost, $dbport, $dbtype, $dbname, $dbuser, $dbpass);
  }
  $form_state['redirect'] = array('admin/settings/watchdog_aggregator/sites');
} // function watchdog_aggregator_admin_form_add_site_submit

/**
 * Validation function for watchdog_aggregator_admin_form_add_site.
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function watchdog_aggregator_admin_form_add_site_validate($form, &$form_state) {
  $url = (string)trim($form_state['values']['url']);
  if (!valid_url($url, TRUE)) {
    form_set_error('url', t('Please enter a valid absolute url!'));
  }
} // function watchdog_aggregator_admin_form_add_site_validate

/**
 * Generates the settings form for the watchdog aggregator.
 *
 * @param array $form_state
 * @return array
 */
function watchdog_aggregator_admin_form_cache($form_state) {
  $form = array();

  $times = array(
    3600   => t('One hour'),
    7200   => t('Two hours'),
    14400  => t('Four hours'),
    28800  => t('Eight hours'),
    43200  => t('Twelve hours'),
    86400  => t('One day'),
    172800 => t('Two days'),
    259200 => t('Three days'),
    345600 => t('Four days'),
    432000 => t('Five days'),
  );
  $form['cache'] = array(
    '#title' => t('Cache settings'),
    '#type' => 'fieldset',
  );
  $form['cache']['watchdog_aggregator_cache_max_age'] = array(
    '#type' => 'select',
    '#title' => t('Maximum age of cached entries'),
    '#description' => t('All entries in the log cache that are older than this will be deleted on the next cron run. Be aware that log entries that are older than the limit you set here will also not be imported!'),
    '#options' => $times,
    '#default_value' => (int)variable_get('watchdog_aggregator_cache_max_age', 172800),
    '#required' => TRUE,
  );

  $levels = array(
    WATCHDOG_EMERG    => t('Emergency: system is unusable'),
    WATCHDOG_ALERT    => t('Alert: action must be taken immediately'),
    WATCHDOG_CRITICAL => t('Critical: critical conditions'),
    WATCHDOG_ERROR    => t('Error: error conditions'),
    WATCHDOG_WARNING  => t('Warning: warning conditions'),
    WATCHDOG_NOTICE   => t('Notice: normal but significant condition'),
  );
  $form['fetch'] = array(
    '#title' => t('Fetch settings'),
    '#type' => 'fieldset',
  );
  $form['fetch']['watchdog_aggregator_fetch_interval'] = array(
    '#type' => 'select',
    '#title' => t('Fetch intervall'),
    '#description' => t('The watchdog aggregator will query all sites and cache their log data in this intervall.'),
    '#options' => $times,
    '#default_value' => (int)variable_get('watchdog_aggregator_fetch_interval', 86400),
    '#required' => TRUE,
  );
  $form['fetch']['watchdog_aggregator_minimum_log_level'] = array(
    '#type' => 'select',
    '#title' => t('The minimum log level to import.'),
    '#description' => t('Select the minimum log level to import here. Usually you set this to "Critical" or "Alert" to prevent massive downloads of log data.'),
    '#options' => $levels,
    '#default_value' => (int)variable_get('watchdog_aggregator_minimum_log_level', WATCHDOG_ERROR),
    '#required' => TRUE,
  );

  return system_settings_form($form);
} // function watchdog_aggregator_admin_form_cache

/**
 * A wrapper for the delete form.
 * 
 * @param int $sid
 * @return string
 */
function watchdog_aggregator_admin_form_delete_callback($sid) {
  return drupal_get_form('watchdog_aggregator_admin_form_delete', $sid);
} // function watchdog_aggregator_admin_form_delete_callback

/**
 * Asks if the user really wants to delete the site configuration.
 *
 * @param array $form_state
 * @param int $sid
 * @return array
 */
function watchdog_aggregator_admin_form_delete($form_state, $sid) {
  $sid = (int)$sid;
  $site = watchdog_aggregator_get_site($sid);
  if (!empty($site)) {
    return confirm_form(
      array(
        'sid' => array('#type' => 'value', '#value' => $site['sid']),
      ),
      t('Are you sure you want to delete the site configuration for %u?', array('%u' => $site['url'])),
      'admin/settings/watchdog_aggregator/site/'. $site['sid']
    );
  }
  return array();
} // function watchdog_aggregator_admin_form_delete

/**
 * Deletes a site configuration and all of it's log entries.
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function watchdog_aggregator_admin_form_delete_submit($form, &$form_state) {
  watchdog_aggregator_delete_site($form_state['values']['sid']);
  $form_state['redirect'] = array('admin/settings/watchdog_aggregator/sites');
} // function watchdog_aggregator_admin_form_delete_submit

/**
 * Generates the form for site administration.
 *
 * @param array $form_state
 * @return array
 */
function watchdog_aggregator_admin_form_sites($form_state) {
  $form = array();

  $sites = watchdog_aggregator_get_sites();
  $stats = array();
  if (!empty($sites)) {
    $form['sites'] = array(
      '#title' => t('Sites'),
      '#type' => 'fieldset',
    );

    $header = array(
      t('Website'),
      t('Fetch logs'),
      t('Last log entry'),
      t('Log entries in cache')
    );
    $rows = array();
    $switch = '';
    foreach ($sites as $site) {
      if ((int)$site['active'] === 0) {
        $switch = t('No');
      }
      else {
        $switch = t('Yes');
      }
      array_push($rows, array(
          l($site['url'], 'admin/settings/watchdog_aggregator/site/'. $site['sid'], array('attributes' => array('title' => t('Click here to view details about this configuration or to edit it.')))),
          l($switch, 'admin/settings/watchdog_aggregator/site/'. $site['sid'] .'/toggle', array('attributes' => array('title' => t('Click here to toggle the fetch status of this site configuration.')))),
          date('Y-m-d H:i:s O', watchdog_aggregator_get_last_sync($site['sid'])),
          watchdog_aggregator_count_log_lines($site['sid']),
        ));
      $stats[$site['sid']] = watchdog_aggregator_get_statistics($site['sid']);
    } // foreach
    $form['sites']['list'] = array(
      '#type' => 'markup',
      '#value' => theme('table', $header, $rows),
    );
    // Statistics
    $plot = array();
    foreach ($stats as $key => $value) {
      $tmp = array();
      foreach ($value as $eid => $count) {
        $tmp[] = array($eid, $count['count']);
      } // foreach
      $p = new flotData($tmp);
      $p->lines->show = FALSE;
      $p->bars->show = TRUE;
      $s = watchdog_aggregator_get_site($key);
      $p->label = $s['url'];
      $plot[] = $p;
    } // foreach
    $options = array(
      'legend' => array('container' => '#watchdog_aggregator_sites_graph_legend'),
      'xaxis' => array(
        'ticks' => array(
          array(WATCHDOG_EMERG + 0.5, t('Emergency')),
          array(WATCHDOG_ALERT + 0.5, t('Alert')),
          array(WATCHDOG_CRITICAL + 0.5, t('Critical')),
          array(WATCHDOG_ERROR + 0.5, t('Error')),
          array(WATCHDOG_WARNING + 0.5, t('Warning')),
          array(WATCHDOG_NOTICE + 0.5, t('Notice')),
        ),
      ),
      'yaxis' => array('min' => 0, 'minTickSize' => 1),
    );
    $form['stats'] = array(
      '#title' => t('Statistics'),
      '#description' => t('The graph below shows the amount of log entries per type for each site.'),
      '#type' => 'fieldset',
    );
    $form['stats']['graph'] = array(
      '#type' => 'markup',
      '#prefix' => '<div>',
      '#value' => theme('flot_graph', array(), $plot, $options),
      '#suffix' => '</div>',
    );
    $form['stats']['legend'] = array(
      '#type' => 'markup',
      '#value' => '<div id="watchdog_aggregator_sites_graph_legend"></div>',
    );
  }
  else {
    drupal_set_message(t('No sites are defined.'));
  }

  return $form;
} // function watchdog_aggregator_admin_form_sites

/**
 * A wrapper callback for editing a site configuration.
 *
 * @param int $sid The id of the site configuration.
 * @return string
 */
function watchdog_aggregator_admin_site_details($sid = 0) {
  $sid = (int)$sid;
  $site = watchdog_aggregator_get_site($sid);
  if (!empty($site)) {
    return drupal_get_form('watchdog_aggregator_admin_form_add_site', $site);
  }
  drupal_set_message(t('No such site configuration!'), 'warning');
  drupal_goto('admin/settings/watchdog_aggregator/sites');
} // function watchdog_aggregator_admin_site_details

/**
 * Generates an overview page for the imported logs.
 * Taken from dblog module.
 *
 * @return string A HTML-Page.
 */
function watchdog_aggregator_overview() {
  $filter = watchdog_aggregator_build_filter_query();
  $rows = array();
  $icons = array(
    WATCHDOG_DEBUG    => '',
    WATCHDOG_INFO     => '',
    WATCHDOG_NOTICE   => '',
    WATCHDOG_WARNING  => theme('image', 'misc/watchdog-warning.png', t('warning'), t('warning')),
    WATCHDOG_ERROR    => theme('image', 'misc/watchdog-error.png', t('error'), t('error')),
    WATCHDOG_CRITICAL => theme('image', 'misc/watchdog-error.png', t('critical'), t('critical')),
    WATCHDOG_ALERT    => theme('image', 'misc/watchdog-error.png', t('alert'), t('alert')),
    WATCHDOG_EMERG    => theme('image', 'misc/watchdog-error.png', t('emergency'), t('emergency')),
  );
  $classes = array(
    WATCHDOG_DEBUG    => 'dblog-debug',
    WATCHDOG_INFO     => 'dblog-info',
    WATCHDOG_NOTICE   => 'dblog-notice',
    WATCHDOG_WARNING  => 'dblog-warning',
    WATCHDOG_ERROR    => 'dblog-error',
    WATCHDOG_CRITICAL => 'dblog-critical',
    WATCHDOG_ALERT    => 'dblog-alert',
    WATCHDOG_EMERG    => 'dblog-emerg',
  );

  $output = drupal_get_form('watchdog_aggregator_filter_form');

  $header = array(
    ' ',
    array('data' => t('Type'), 'field' => 'w.type'),
    array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
    t('Message'),
    array('data' => t('Website'), 'field' => 'u.url'),
    array('data' => t('Operations')),
  );

  $sql = "SELECT w.wid, w.sid, w.severity, w.type, w.timestamp, w.message, w.variables, w.link, u.url FROM {watchdog_aggregator_logs} w INNER JOIN {watchdog_aggregator_sites} u ON w.sid = u.sid";
  $tablesort = tablesort_sql($header);
  if (!empty($filter['where'])) {
    $result = pager_query($sql ." WHERE ". $filter['where'] . $tablesort, 50, 0, NULL, $filter['args']);
  }
  else {
    $result = pager_query($sql . $tablesort, 50);
  }

  while ($dblog = db_fetch_object($result)) {
    $rows[] = array('data' =>
      array(
        // Cells
        $icons[$dblog->severity],
        t($dblog->type),
        format_date($dblog->timestamp, 'small'),
        l(truncate_utf8(_watchdog_aggregator_format_message($dblog), 56, TRUE, TRUE), 'admin/reports/watchdog_aggregator/event/'. $dblog->wid, array('html' => TRUE)),
        l($dblog->url, $dblog->url, array('attributes' => array('target' => '_blank'))),
        $dblog->link,
      ),
      // Attributes for tr
      'class' => "dblog-". preg_replace('/[^a-z]/i', '-', $dblog->type) .' '. $classes[$dblog->severity]
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No log messages available.'), 'colspan' => 6));
  }

  $output .= '<div style="overflow-x: scroll;">';
  $output .= theme('table', $header, $rows, array('id' => 'admin-dblog'));
  $output .= '</div>';
  $output .= theme('pager', NULL, 50, 0);

  return $output;
} // function watchdog_aggregator_overview

/**
 * Build query for dblog administration filters based on session.
 * Taken from dblog module.
 *
 * @return mixed
 */
function watchdog_aggregator_build_filter_query() {
  if (empty($_SESSION['watchdog_aggregator_overview_filter'])) {
    return;
  }

  $filters = watchdog_aggregator_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['watchdog_aggregator_overview_filter'] as $key => $filter) {
    $filter_where = array();
    foreach ($filter as $value) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
} // function watchdog_aggregator_build_filter_query

/**
 * List wd administration filters that can be applied.
 * Taken from dblog module.
 *
 * @return array
 */
function watchdog_aggregator_filters() {
  $filters = array();

  foreach (_watchdog_aggregator_get_message_types() as $type) {
    $types[$type] = $type;
  }

  if (!empty($types)) {
    $filters['type'] = array(
      'title' => t('Type'),
      'where' => "w.type = '%s'",
      'options' => $types,
    );
  }

  $filters['severity'] = array(
    'title' => t('Severity'),
    'where' => 'w.severity = %d',
    'options' => watchdog_severity_levels(),
  );

  $sites = watchdog_aggregator_get_sites();
  if (!empty($sites)) {
    $ids = array();
    foreach ($sites as $site) {
      $ids[$site['sid']] = $site['url'];
    } // foreach
    $filters['site'] = array(
      'title' => t('Website'),
      'where' => 'w.sid = %d',
      'options' => $ids,
    );
  }

  return $filters;
} // function watchdog_aggregator_filters

/**
 * Return form for watchdog_aggregator administration filters.
 * Taken from dblog module.
 *
 * @ingroup forms
 * @see watchdog_aggregator_filter_form_submit()
 * @see watchdog_aggregator_filter_form_validate()
 */
function watchdog_aggregator_filter_form() {
  $session = &$_SESSION['watchdog_aggregator_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = watchdog_aggregator_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#theme' => 'watchdog_aggregator_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filter['options'],
    );
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from watchdog_aggregator administration filter form.
 * Taken from dblog module.
 */
function watchdog_aggregator_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['type']) && empty($form_state['values']['severity']) && empty($form_state['values']['site'])) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from watchdog_aggregator administration filter form.
 * Taken from dblog module.
 */
function watchdog_aggregator_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = watchdog_aggregator_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['watchdog_aggregator_overview_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['watchdog_aggregator_overview_filter'] = array();
      break;
  }
  return 'admin/reports/watchdog_aggregator';
}
